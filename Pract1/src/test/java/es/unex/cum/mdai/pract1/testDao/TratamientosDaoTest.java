package es.unex.cum.mdai.pract1.testDao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import es.unex.cum.mdai.clasesdao.PacientesDAO;
import es.unex.cum.mdai.clasesdao.TratamientosDAO;
import es.unex.cum.mdai.clasesimpl.PacientesDAOImpl;
import es.unex.cum.mdai.clasesimpl.TratamientosDAOImpl;
import es.unex.cum.mdai.pract1.clases.Pacientes;
import es.unex.cum.mdai.pract1.clases.Tratamiento;

public class TratamientosDaoTest {
	private static EntityManagerFactory emf;
	private EntityManager em;

	@BeforeClass
	public static void createEntityManagerFactory() {
		emf=Persistence.createEntityManagerFactory("UnidadPersistencia");
	}

	@AfterClass
	public static void closeEntityManagerFactory() {
		emf.close();
	}

	@Before
	public void beginTransaction() {
		em=emf.createEntityManager();
		em.getTransaction().begin();
	}

	@After
	public void rollbackTransaction() {
		if (em.getTransaction().isActive()) {
			em.getTransaction().commit();
		}
		if(em.isOpen()) {
			em.close();
		}
	}

	/**
	 * En este metodo nos creamos un paciente y varios tratamientos y a�adimos la lista de tratamientos al pacientes
	 */
	@Test
	public void addTratamientoAPaciente () {

		List<Tratamiento> listaTratamientos = new ArrayList <Tratamiento>();
		TratamientosDAO tratamientosDao=new TratamientosDAOImpl (em);
		Tratamiento tratamiento=new Tratamiento();
		Tratamiento tratamiento2;

		PacientesDAO pDAO=new PacientesDAOImpl(em);
		Pacientes p=new Pacientes();
		Pacientes p2;

		//creamos un tratamiento
		tratamiento.setNombreTratamiento("Paracetamol");
		tratamiento.setDosis(2);
		tratamiento.setVecesDia(3);

		tratamientosDao.create(tratamiento);

		tratamiento2=tratamientosDao.read(1);

		System.out.println("NOMBRE TRATAMIENTO: "+tratamiento2.getNombreTratamiento()+" VECES AL D�A: "+tratamiento2.getVecesDia()+" DOSIS: "+tratamiento2.getDosis());

		listaTratamientos.add(tratamiento2);


		p.setNombre("Sara");
		p.setApellidos("Gomez");
		p.setEdad(91);
		p.setListaTratamientos(listaTratamientos); //a�adimos la lista de tratamientos al paciente

		pDAO.create(p);

		p2=pDAO.read(1);

		System.out.println("NOMBRE PACIENTE: "+p2.getNombre()+" APELLIDOS PACIENTE: "+p2.getApellidos()+" EDAD PACIENTE: "+p2.getEdad()+" TRATAMIENTOS: "+p.getListaTratamientos().get(0).getNombreTratamiento());


	}
	/**
	 * En este metodo nos creamos un paciente y a la lista de tratamientos le a�adimos dicho paciente
	 */
	@Test
	public void addPacienteATratamiento () {
		List<Pacientes> listaPacientes = new ArrayList <Pacientes>();
		//Nos creamos un paciente
		PacientesDAO pDAO=new PacientesDAOImpl(em);
		Pacientes p=new Pacientes();
		Pacientes p2;
		TratamientosDAO tratamientosDao=new TratamientosDAOImpl (em);
		Tratamiento tratamiento=new Tratamiento();
		Tratamiento tratamiento2;

		p.setNombre("Juan");
		p.setApellidos("Perez");
		p.setEdad(84);
		p.setListaTratamientos(null);
		pDAO.create(p);

		p2=pDAO.read(1);

		listaPacientes.add(p2); //A�adimos el paciente a la lista de apcientes
		System.out.println("NOMBRE PACIENTE: "+p2.getNombre()+" APELLIDOS PACIENTE: "+p2.getApellidos()+" EDAD PACIENTE: "+p2.getEdad());

		//creamos un tratamiento
		tratamiento.setNombreTratamiento("Paracetamol");
		tratamiento.setDosis(2);
		tratamiento.setVecesDia(3);
		tratamiento.setListaPacientes(listaPacientes);
		
		tratamientosDao.create(tratamiento);

		tratamiento2=tratamientosDao.read(1);
		System.out.println("NOMBRE TRATAMIENTO: "+tratamiento2.getNombreTratamiento()+" VECES AL D�A: "+tratamiento2.getVecesDia()+" DOSIS: "+tratamiento2.getDosis()+" PACIENTES: "+tratamiento2.getListaPacientes().get(0).getNombre());


	}
}
