package es.unex.cum.mdai.pract1.testDao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import es.unex.cum.mdai.clasesdao.FamiliaresDAO;
import es.unex.cum.mdai.clasesdao.PacientesDAO;
import es.unex.cum.mdai.clasesimpl.FamiliaresDAOImpl;
import es.unex.cum.mdai.clasesimpl.PacientesDAOImpl;
import es.unex.cum.mdai.pract1.clases.Familiares;
import es.unex.cum.mdai.pract1.clases.Pacientes;

public class FamiliaresDaoTest {
	private static EntityManagerFactory emf;
	private EntityManager em;

	@BeforeClass
	public static void createEntityManagerFactory() {
		emf=Persistence.createEntityManagerFactory("UnidadPersistencia");
	}

	@AfterClass
	public static void closeEntityManagerFactory() {
		emf.close();
	}

	@Before
	public void beginTransaction() {
		em=emf.createEntityManager();
		em.getTransaction().begin();
	}

	@After
	public void rollbackTransaction() {
		if (em.getTransaction().isActive()) {
			em.getTransaction().commit();
		}
		if(em.isOpen()) {
			em.close();
		}
	}
	/**
	 * En este metodo creamos un familiar y se lo a�adimos al paciente
	 */
	@Test
	public void addFamiliarAPaciente () {
		FamiliaresDAO fDAO=new FamiliaresDAOImpl (em);
		Familiares f=new Familiares();
		Familiares f2;
		PacientesDAO pDAO=new PacientesDAOImpl (em);
		Pacientes p=new Pacientes();
		Pacientes p2;
		List<Familiares> listaFamiliares=new ArrayList<Familiares>();
		
		//Creamos familiar
		f.setNombre("Clara");
		f.setApellidos("Dominguez");
		f.setParentesco("Hija");
		fDAO.create(f);
		f2=fDAO.read(1);
		
		System.out.println("NOMBRE FAMILIAR: "+f2.getNombre()+" APELLIDOS FAMILIAR: "+f2.getApellidos()+" PARENTESCO FAMILIAR: "+f2.getParentesco());
		
		listaFamiliares.add(f2);
		p.setNombre("Pepa");
		p.setApellidos("Dominguez");
		p.setEdad(98);
		p.setListaFamiliares(listaFamiliares);
		p2=pDAO.read(1);
		
		System.out.println("NOMBRE PACIENTE: "+p2.getNombre()+" APELLIDOS PACIENTE: "+p2.getApellidos()+" EDAD PACIENTE: "+p2.getEdad()+" FAMILIAR: "+p2.getListaFamiliares().get(0).getNombre());
		
		
	}
}
