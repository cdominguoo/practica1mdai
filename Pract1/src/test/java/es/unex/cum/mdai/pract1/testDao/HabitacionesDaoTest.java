package es.unex.cum.mdai.pract1.testDao;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import es.unex.cum.mdai.clasesdao.CentrosDAO;
import es.unex.cum.mdai.clasesdao.HabitacionesDAO;
import es.unex.cum.mdai.clasesimpl.CentrosDAOImpl;
import es.unex.cum.mdai.clasesimpl.HabitacionesDAOImpl;
import es.unex.cum.mdai.pract1.clases.Centros;
import es.unex.cum.mdai.pract1.clases.Habitaciones;



public class HabitacionesDaoTest {
	private static EntityManagerFactory emf;
	private EntityManager em;

	@BeforeClass
	public static void createEntityManagerFactory() {
		emf=Persistence.createEntityManagerFactory("UnidadPersistencia");
	}
	
	@AfterClass
	public static void closeEntityManagerFactory() {
		emf.close();
	}
	
	@Before
	public void beginTransaction() {
		em=emf.createEntityManager();
		em.getTransaction().begin();
	}
	
	@After
	public void rollbackTransaction() {
		if (em.getTransaction().isActive()) {
			em.getTransaction().commit();
		}
		if(em.isOpen()) {
			em.close();
		}
	}
	
	/**
	 * A�adimos una habitacion a la base de datos
	 */
	@Test
	public void addHabitacion () {

		HabitacionesDAO habitacionesDao=new HabitacionesDAOImpl (em);
		Habitaciones hab=new Habitaciones();
		hab.setNumPacientesHabitacion(2);
		hab.setPlanta(5);
		habitacionesDao.create(hab);

		System.out.println("ID HABITACION: "+hab.getIdHabitacion()+" NUM PACIENTES: "+hab.getnumPacientesHabitacion()+" NUM PLANTA: "+hab.getPlanta());

	}
	/**
	 * Modificamos habitacion
	 */
	@Test
	public void modificarHabitacion() {
		HabitacionesDAO habitacionesDao=new HabitacionesDAOImpl (em);
		Habitaciones hab2;
		hab2=habitacionesDao.read(1);
		hab2.setNumPacientesHabitacion(1); //Modificamos el numero de pacientes
		habitacionesDao.update(hab2);
		
		System.out.println("ID HABITACION: "+hab2.getIdHabitacion()+" NUM PACIENTES: "+hab2.getnumPacientesHabitacion()+" NUM PLANTA: "+hab2.getPlanta());

	}
	
	/**
	 * A�adimos una habitacion a la base de datos y la eliminamos
	 */
	@Test
	public void addHabitacionElimina () {

		HabitacionesDAO habitacionesDao=new HabitacionesDAOImpl (em);
		Habitaciones hab=new Habitaciones();
		Habitaciones h;
		hab.setNumPacientesHabitacion(1);
		hab.setPlanta(6);
		habitacionesDao.create(hab);

		System.out.println("ID HABITACION: "+hab.getIdHabitacion()+" NUM PACIENTES: "+hab.getnumPacientesHabitacion()+" NUM PLANTA: "+hab.getPlanta());
		
		habitacionesDao.delete(hab);
		
		h=habitacionesDao.read(2); //Recuperamos la habitacion con el id 2
		if (h==null) {
			System.out.println("Se ha eliminado la habitacion. ");
		}
	}
	@Test
	public void addHabitacionACentro() {
		HabitacionesDAO habDao=new HabitacionesDAOImpl(em);
		Habitaciones hab=new Habitaciones();
		
		CentrosDAO centrosDao=new CentrosDAOImpl (em);
		Centros centro=new Centros();
		Centros c;
		
		centro.setNombreCentro("Centro de ancianos Rosalba.");
		centro.setNumPacientes(83);
		centrosDao.create(centro);

		System.out.println("ID CENTRO: "+centro.getIdCentro()+" NOMBRE: "+centro.getNombreCentro()+" NUM PACIENTES: "+centro.getNumPacientes());

		c=centrosDao.read(1); //recuperamos el centro
		
		hab.setCentro(c); //a�adimos la habitacion al centro
		hab.setNumPacientesHabitacion(2);
		hab.setPlanta(4);
		habDao.create(hab);
		
		System.out.println("ID HABITACION: "+hab.getIdHabitacion()+" NUM PACIENTES: "+hab.getnumPacientesHabitacion()+" NUM PLANTA: "+hab.getPlanta()+" NOMBRE CENTRO AL QUE PERTENECE LA HABITACION: "+hab.getCentro().getNombreCentro());
		
	}
	
}
