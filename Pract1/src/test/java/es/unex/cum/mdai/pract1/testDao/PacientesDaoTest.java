package es.unex.cum.mdai.pract1.testDao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import es.unex.cum.mdai.clasesdao.CentrosDAO;
import es.unex.cum.mdai.clasesdao.HabitacionesDAO;
import es.unex.cum.mdai.clasesdao.PacientesDAO;
import es.unex.cum.mdai.clasesimpl.CentrosDAOImpl;
import es.unex.cum.mdai.clasesimpl.HabitacionesDAOImpl;
import es.unex.cum.mdai.clasesimpl.PacientesDAOImpl;
import es.unex.cum.mdai.pract1.clases.Centros;
import es.unex.cum.mdai.pract1.clases.Habitaciones;
import es.unex.cum.mdai.pract1.clases.Pacientes;


public class PacientesDaoTest {
	private static EntityManagerFactory emf;
	private EntityManager em;

	@BeforeClass
	public static void createEntityManagerFactory() {
		emf=Persistence.createEntityManagerFactory("UnidadPersistencia");
	}

	@AfterClass
	public static void closeEntityManagerFactory() {
		emf.close();
	}

	@Before
	public void beginTransaction() {
		em=emf.createEntityManager();
		em.getTransaction().begin();
	}

	@After
	public void rollbackTransaction() {
		if (em.getTransaction().isActive()) {
			em.getTransaction().commit();
		}
		if(em.isOpen()) {
			em.close();
		}
	}
	/**
	 * Creamos un centro, le a�adimos una habitacion y a esa habitacion le a�adimos un paciente
	 */
	@Test
	public void addPaciente() {
		PacientesDAO pDAO=new PacientesDAOImpl (em);
		Pacientes p=new Pacientes();
		
		HabitacionesDAO habDAO=new HabitacionesDAOImpl(em);
		Habitaciones hab=new Habitaciones();
		CentrosDAO cDAO=new CentrosDAOImpl(em);
		Centros c=new Centros();
		
		c.setNombreCentro("Centro de d�a M�rida");
		c.setNumPacientes(200);
		
		cDAO.create(c);
		
		System.out.println("NOMBRE CENTRO: "+c.getNombreCentro());
		
		hab.setPlanta(2);
		hab.setNumPacientesHabitacion(2);
		hab.setCentro(c); //A�adimos la hab al centro
		
		habDAO.create(hab);
		
		System.out.println("NOMBRE CENTRO PERTENECIENTE HABITACION: "+hab.getCentro().getNombreCentro());
		
		p.setNombre("Juan");
		p.setApellidos("Perez");
		p.setEdad(84);
		p.setHabitacion(hab); //A�adimos a habitacion al paciente
	
		
		System.out.println("ID HABITACION: "+hab.getIdHabitacion());
		
		pDAO.create(p);
		
		
		System.out.println("ID PACIENTE: "+p.getIdUsuario()+" NOMBRE PACIENTE: "+p.getNombre()+" APELLIDOS PACIENTE: "+p.getApellidos()+" EDAD PACIENTE: "+p.getEdad()+" ID HABITACION PACIENTE: "+p.getHabitacion().getIdHabitacion());
	}
	/**
	 * Nos creamos dos pacientes y buscamos cual de ellos es el mayor
	 */
	@Test
	public void maxEdad() {
		PacientesDAO pDAO=new PacientesDAOImpl (em);
		Pacientes p=new Pacientes();
		Pacientes p2=new Pacientes();
		Pacientes pacienteAnciano;
		p.setNombre("Juan");
		p.setApellidos("Perez");
		p.setEdad(84);
		
		pDAO.create(p);
		
		p2.setNombre("Luis");
		p2.setApellidos("Garcia");
		p2.setEdad(99);
		
		pDAO.create(p2);
		
		pacienteAnciano=pDAO.pacienteMasAnciano();
		
		System.out.println("El paciente m�s anciano es "+pacienteAnciano.getNombre()+" con "+pacienteAnciano.getEdad()+" a�os.");
	}
//Relacion paciente-centro
	
	//listar los pacientes de un centro
	@Test
	public void listarPacientesCentro() {
		PacientesDAO pDAO=new PacientesDAOImpl (em);
		Pacientes p=new Pacientes();
		Pacientes p2=new Pacientes();
		List<Pacientes> listaPacientes;
		CentrosDAO cDAO=new CentrosDAOImpl(em);
		Centros c=new Centros();
		
		c.setNombreCentro("Centro de d�a M�rida");
		c.setNumPacientes(200);
		
		cDAO.create(c);
		
		p.setNombre("Juan");
		p.setApellidos("Perez");
		p.setEdad(84);
		p.setCentro(c);
		
		pDAO.create(p);
		
		p2.setNombre("Sara");
		p2.setApellidos("Lopez");
		p2.setEdad(88);
		p2.setCentro(c);
		
		pDAO.create(p2);
		
		listaPacientes=pDAO.listarPacientesCentro(c);
		
		System.out.println("PACIENTES CENTRO "+c.getNombreCentro());
		for (int i=0; i<listaPacientes.size(); i++) {
			System.out.println(listaPacientes.get(i).getNombre()+" "+listaPacientes.get(i).getApellidos()+" "+listaPacientes.get(i).getEdad());
		}
		
	}
}
