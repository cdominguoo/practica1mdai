package es.unex.cum.mdai.pract1.testDao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import org.junit.FixMethodOrder;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import es.unex.cum.mdai.clasesdao.CentrosDAO;
import es.unex.cum.mdai.clasesdao.TrabajadoresDAO;
import es.unex.cum.mdai.clasesimpl.CentrosDAOImpl;
import es.unex.cum.mdai.clasesimpl.TrabajadoresDAOImpl;
import es.unex.cum.mdai.pract1.clases.Centros;
import es.unex.cum.mdai.pract1.clases.Trabajadores;
@FixMethodOrder
public class CentrosDaoTest {
	private static EntityManagerFactory emf;
	private EntityManager em;

	@BeforeClass
	public static void createEntityManagerFactory() {
		emf=Persistence.createEntityManagerFactory("UnidadPersistencia");
	}

	@AfterClass
	public static void closeEntityManagerFactory() {
		emf.close();
	}

	@Before
	public void beginTransaction() {
		em=emf.createEntityManager();
		em.getTransaction().begin();
	}

	@After
	public void rollbackTransaction() {
		if (em.getTransaction().isActive()) {
			em.getTransaction().commit();
		}
		if(em.isOpen()) {
			em.close();
		}
	}
	/**
	 * A�adimos un centro a la base de datos
	 */
	@Test
	public void addCentro () {

		CentrosDAO centrosDao=new CentrosDAOImpl (em);
		Centros centro=new Centros();
		centro.setNombreCentro("Centro de ancianos Rosalba.");
		centro.setNumPacientes(83);
		
		centrosDao.create(centro);

		System.out.println("ID CENTRO: "+centro.getIdCentro()+" NOMBRE: "+centro.getNombreCentro()+" NUM PACIENTES: "+centro.getNumPacientes());


	}
	/**
	 * Recuperamos un centro y le a�adimos un trabajador al centro
	 */
	@Test
	public void addTrabajadorACentro() {
		TrabajadoresDAO trabajadoresDao=new TrabajadoresDAOImpl (em);
		Centros centro2;
		CentrosDAO centrosDao=new CentrosDAOImpl (em);

		centro2=centrosDao.read(1); //Recuperamos el centro

		Trabajadores trabajador2 = new Trabajadores();
		trabajador2.setCentro(centro2); //a�adimos al trabajador el centro que nos hemos creado
		trabajador2.setNombre("Luisa");
		trabajador2.setApellidos("Sanchez");

		trabajadoresDao.create(trabajador2);

		System.out.println("Nombre trabajador: "+trabajador2.getNombre()+" Apellidos trabajador: "+trabajador2.getApellidos()+" Centro trabajador: "+trabajador2.getCentro().getIdCentro()+", "+trabajador2.getCentro().getNombreCentro());
	}
	@Test
	public void listarTrabajadoresCentro() {
		Centros c;
		CentrosDAO centrosDao=new CentrosDAOImpl (em);
		c=centrosDao.read(1); //recuperamos el centro
	
		for (int i=0; i<c.getListaTrabajadores().size(); i++) {

			System.out.println(c.getListaTrabajadores().get(i).getNombre()+" "+c.getListaTrabajadores().get(i).getApellidos());
		}
	}
	@Test
	public void listarCentros() {
		List<String> listaCentros;
		CentrosDAO centrosDao=new CentrosDAOImpl (em);
		Centros centro=new Centros();
		Centros centro2=new Centros();
		
		centro.setNombreCentro("Centro de ancianos Rosalba.");
		centro.setNumPacientes(83);
		centrosDao.create(centro);
		
		centro2.setNombreCentro("Centro de ancianos Nueva Ciudad.");
		centro2.setNumPacientes(214);
		centrosDao.create(centro2);
		
		listaCentros=centrosDao.listarCentros();
		for (int i=0; i<listaCentros.size();i++) {
			System.out.println(listaCentros.get(i));
		}
	}


}
