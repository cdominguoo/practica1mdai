package es.unex.cum.mdai.pract1.testDao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import es.unex.cum.mdai.clasesdao.CentrosDAO;
import es.unex.cum.mdai.clasesdao.TrabajadoresDAO;
import es.unex.cum.mdai.clasesimpl.CentrosDAOImpl;
import es.unex.cum.mdai.clasesimpl.TrabajadoresDAOImpl;
import es.unex.cum.mdai.pract1.clases.Centros;
import es.unex.cum.mdai.pract1.clases.Trabajadores;

public class TrabajadoresDaoTest {
	private static EntityManagerFactory emf;
	private EntityManager em;

	@BeforeClass
	public static void createEntityManagerFactory() {
		emf=Persistence.createEntityManagerFactory("UnidadPersistencia");
	}

	@AfterClass
	public static void closeEntityManagerFactory() {
		emf.close();
	}

	@Before
	public void beginTransaction() {
		em=emf.createEntityManager();
		em.getTransaction().begin();
	}

	@After
	public void rollbackTransaction() {
		if (em.getTransaction().isActive()) {
			em.getTransaction().commit();
		}
		if(em.isOpen()) {
			em.close();
		}
	}
	/**
	 * Creamos varios trabajadores y luego los mostramos alfabeticamente por su nombre
	 */
	@Test
	public void crearTrabajadores() {
		List<Trabajadores> listaTrabajadores;
		TrabajadoresDAO tDAO=new TrabajadoresDAOImpl (em);
		Trabajadores t=new Trabajadores();
		Trabajadores t2=new Trabajadores();
		CentrosDAO centrosDao=new CentrosDAOImpl (em);
		Centros centro=new Centros();

		centro.setNombreCentro("Centro de ancianos Rosalba.");
		centro.setNumPacientes(83);
		centrosDao.create(centro);

		t.setNombre("Alba");
		t.setApellidos("Sanchez");
		t.setCentro(centro);

		tDAO.create(t);

		t2.setNombre("Clara");
		t2.setApellidos("Lopez");
		t2.setCentro(centro);

		tDAO.create(t2);

		listaTrabajadores=tDAO.listarAlfabeticamente();
		
		System.out.println("Lista trabajadores ordenada alfabeticamente: ");
		for (int i=0; i<listaTrabajadores.size();i++) {
			System.out.println(listaTrabajadores.get(i).getNombre()+" "+listaTrabajadores.get(i).getApellidos()+" "+listaTrabajadores.get(i).getCentro().getNombreCentro());
		}
	}
}
