package es.unex.cum.mdai.clasesimpl;

import javax.persistence.EntityManager;

import es.unex.cum.mdai.clasesdao.TratamientosDAO;
import es.unex.cum.mdai.pract1.clases.Tratamiento;

public class TratamientosDAOImpl implements TratamientosDAO {
	protected EntityManager entityManager;
	
	public TratamientosDAOImpl (EntityManager entityManager) {
		this.entityManager=entityManager;
	}
	public Tratamiento create (Tratamiento tratamiento) {
		this.entityManager.persist(tratamiento);
		return tratamiento;
	}
	public Tratamiento read (int idTratamiento) {
		return this.entityManager.find(Tratamiento.class, idTratamiento);
	}
	public Tratamiento update (Tratamiento tratamiento) {
		return this.entityManager.merge(tratamiento);
	}
	public void delete (Tratamiento tratamiento) {
		tratamiento=this.entityManager.merge(tratamiento);
		this.entityManager.remove(tratamiento);
	}
}
