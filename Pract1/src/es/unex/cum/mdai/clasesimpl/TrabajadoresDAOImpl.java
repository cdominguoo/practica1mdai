package es.unex.cum.mdai.clasesimpl;

import java.util.List;

import javax.persistence.EntityManager;

import es.unex.cum.mdai.clasesdao.TrabajadoresDAO;
import es.unex.cum.mdai.pract1.clases.Trabajadores;

public class TrabajadoresDAOImpl implements TrabajadoresDAO{
	protected EntityManager entityManager;
	
	public TrabajadoresDAOImpl (EntityManager entityManager) {
		this.entityManager=entityManager;
	}
	public Trabajadores create (Trabajadores trabajador) {
		this.entityManager.persist(trabajador);
		return trabajador;
	}
	public Trabajadores read (int idTrabajador) {
		return this.entityManager.find(Trabajadores.class, idTrabajador);
	}
	public Trabajadores update (Trabajadores trabajador) {
		return this.entityManager.merge(trabajador);
	}
	public void delete (Trabajadores trabajador) {
		trabajador=this.entityManager.merge(trabajador);
		this.entityManager.remove(trabajador);
	}
	public List<Trabajadores> listarAlfabeticamente() {
		List<Trabajadores> resultados;
		 String sentencia="Select t from Trabajadores t order by t.nombre asc";//La tabla tiene que ser la primera en mayuscula y el resto en minuscula, sino da error!!!!
		 javax.persistence.Query query= entityManager.createQuery(sentencia);
		 resultados= ((javax.persistence.Query) query).getResultList();
//		 for(Trabajadores t : resultados) { 
//	            System.out.println(t.getNombre()); 
//	      }
		 return resultados;
	}
}
