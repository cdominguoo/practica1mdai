package es.unex.cum.mdai.clasesimpl;

import java.util.List;

import javax.persistence.EntityManager;

import es.unex.cum.mdai.clasesdao.PacientesDAO;
import es.unex.cum.mdai.pract1.clases.Centros;
import es.unex.cum.mdai.pract1.clases.Pacientes;

public class PacientesDAOImpl implements PacientesDAO{
	protected EntityManager entityManager;

	public PacientesDAOImpl (EntityManager entityManager) {
		this.entityManager=entityManager;
	}
	public Pacientes create (Pacientes p) {
		this.entityManager.persist(p);
		return p;
	}
	public Pacientes read (int idP) {
		return this.entityManager.find(Pacientes.class, idP);
	}
	public Pacientes update (Pacientes p) {
		return this.entityManager.merge(p);
	}
	public void delete (Pacientes p) {
		p=this.entityManager.merge(p);
		this.entityManager.remove(p);
	}
	public Pacientes pacienteMasAnciano () {

		int edad;
		Pacientes paciente;
		String sentencia="Select MAX(p.edad) from Pacientes p";//La tabla tiene que ser la primera en mayuscula y el resto en minuscula, sino da error!!!!
		javax.persistence.Query query= entityManager.createQuery(sentencia);
		edad=(Integer) query.getSingleResult();


		String sentencia2="Select p from Pacientes p where p.edad = :edad";
		javax.persistence.Query query2= entityManager.createQuery(sentencia2);
		query2.setParameter("edad", edad);
		paciente=(Pacientes)query2.getSingleResult();
		return paciente;
	}
	/**
	 * Dado unc entro lista los pacientes pertenecientes a ese centro
	 */
	public List<Pacientes> listarPacientesCentro(Centros centro) {
		List<Pacientes> resultados;

		String sentencia="Select p from Pacientes p where p.centro = :nombreCentro";//La tabla tiene que ser la primera en mayuscula y el resto en minuscula, sino da error!!!!
		javax.persistence.Query query= entityManager.createQuery(sentencia);

		query.setParameter("nombreCentro", centro);
		resultados= ((javax.persistence.Query) query).getResultList();
		//		for(Pacientes t : resultados) { 
		//			System.out.println(t.getNombre()); 
		//		}
		return resultados;
	}
}
