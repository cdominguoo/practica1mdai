package es.unex.cum.mdai.clasesimpl;

import javax.persistence.EntityManager;

import es.unex.cum.mdai.clasesdao.FamiliaresDAO;
import es.unex.cum.mdai.pract1.clases.Familiares;

public class FamiliaresDAOImpl implements FamiliaresDAO{
	protected EntityManager entityManager;
	
	public FamiliaresDAOImpl (EntityManager entityManager) {
		this.entityManager=entityManager;
	}
	public Familiares create (Familiares f) {
		this.entityManager.persist(f);
		return f;
	}
	public Familiares read (int idF) {
		return this.entityManager.find(Familiares.class, idF);
	}
	public Familiares update (Familiares f) {
		return this.entityManager.merge(f);
	}
	public void delete (Familiares f) {
		f=this.entityManager.merge(f);
		this.entityManager.remove(f);
	}

}
