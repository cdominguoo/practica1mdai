package es.unex.cum.mdai.clasesimpl;

import java.util.List;

import javax.persistence.EntityManager;


import es.unex.cum.mdai.clasesdao.CentrosDAO;
import es.unex.cum.mdai.pract1.clases.Centros;

public  class CentrosDAOImpl implements CentrosDAO{
	
	protected EntityManager entityManager;
	
	public CentrosDAOImpl (EntityManager entityManager) {
		this.entityManager=entityManager;
	}
	public Centros create (Centros centro) {
		this.entityManager.persist(centro);
		return centro;
	}
	public Centros read (int idCentro) {
		return this.entityManager.find(Centros.class, idCentro);
	}
	public Centros update (Centros centro) {
		return this.entityManager.merge(centro);
	}
	public void delete (Centros centro) {
		centro=this.entityManager.merge(centro);
		this.entityManager.remove(centro);
	}

	public List<String> listarCentros() {
		List<String> resultados;
		 String sentencia="Select c.nombreCentro from Centros c";//La tabla tiene que ser la primera en mayuscula y el resto en minuscula, sino da error!!!!
		 javax.persistence.Query query= entityManager.createQuery(sentencia);
		 resultados= ((javax.persistence.Query) query).getResultList();
//		 for(String c : resultados) { 
//	            System.out.println(c); 
//	        }
		 return resultados;
	}
	
}
