package es.unex.cum.mdai.clasesimpl;


import javax.persistence.EntityManager;

import es.unex.cum.mdai.clasesdao.HabitacionesDAO;
import es.unex.cum.mdai.pract1.clases.Habitaciones;
 

public class HabitacionesDAOImpl implements HabitacionesDAO {
	protected EntityManager entityManager;
	
	public HabitacionesDAOImpl (EntityManager entityManager) {
		this.entityManager=entityManager;
	}
	public Habitaciones create (Habitaciones habitacion) {
		this.entityManager.persist(habitacion);
		return habitacion;
	}
	public Habitaciones read (int idHabitacion) {
		return this.entityManager.find(Habitaciones.class, idHabitacion);
	}
	public Habitaciones update (Habitaciones habitacion) {
		return this.entityManager.merge(habitacion);
	}
	public void delete (Habitaciones habitacion) {
		habitacion=this.entityManager.merge(habitacion);
		this.entityManager.remove(habitacion);
	}
	
}
