package es.unex.cum.mdai.clasesdao;

import java.util.List;

import es.unex.cum.mdai.pract1.clases.Trabajadores;

public interface TrabajadoresDAO {
	Trabajadores create (Trabajadores trabajadores);
	Trabajadores read (int idTrabajador);
	Trabajadores update (Trabajadores trabajadores);
	void delete (Trabajadores trabajadores);
	List<Trabajadores> listarAlfabeticamente();
}
