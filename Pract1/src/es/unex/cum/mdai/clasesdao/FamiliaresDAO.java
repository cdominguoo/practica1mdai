package es.unex.cum.mdai.clasesdao;

import es.unex.cum.mdai.pract1.clases.Familiares;

public interface FamiliaresDAO {
	Familiares create (Familiares f);
	Familiares read (int idF);
	Familiares update (Familiares f);
	void delete (Familiares f);
}
