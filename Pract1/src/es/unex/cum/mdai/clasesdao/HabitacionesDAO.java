package es.unex.cum.mdai.clasesdao;


import es.unex.cum.mdai.pract1.clases.Habitaciones;

public interface HabitacionesDAO {
	Habitaciones create (Habitaciones habitacion);
	Habitaciones read (int idHabitacion);
	Habitaciones update (Habitaciones habitacion);
	void delete (Habitaciones habitacion);

}
