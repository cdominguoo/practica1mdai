package es.unex.cum.mdai.clasesdao;

import es.unex.cum.mdai.pract1.clases.Tratamiento;

public interface TratamientosDAO {
	Tratamiento create (Tratamiento tratamiento);
	Tratamiento read (int idTratamiento);
	Tratamiento update (Tratamiento tratamiento);
	void delete (Tratamiento tratamiento);
}
