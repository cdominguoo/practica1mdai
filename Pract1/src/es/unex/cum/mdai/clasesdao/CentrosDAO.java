package es.unex.cum.mdai.clasesdao;

import java.util.List;

import es.unex.cum.mdai.pract1.clases.Centros;
import es.unex.cum.mdai.pract1.clases.Trabajadores;

public interface CentrosDAO {
	Centros create (Centros centro);
	Centros read (int idCentro);
	Centros update (Centros centro);
	void delete (Centros centro);
	List<String> listarCentros ();
}
