package es.unex.cum.mdai.clasesdao;

import java.util.List;

import es.unex.cum.mdai.pract1.clases.Centros;
import es.unex.cum.mdai.pract1.clases.Pacientes;

public interface PacientesDAO {
	Pacientes create (Pacientes p);
	Pacientes read (int idP);
	Pacientes update (Pacientes p);
	void delete (Pacientes p);
	Pacientes pacienteMasAnciano ();
	List<Pacientes> listarPacientesCentro(Centros centro);
}
