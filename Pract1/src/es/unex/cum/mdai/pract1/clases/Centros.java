package es.unex.cum.mdai.pract1.clases;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;


@Entity
@Table(name = "CENTROS")
public class Centros {
	@Column(name="idUsuario")
	private int idUsuario;
	
	@Id
	@GeneratedValue(generator = "increment")
	@GenericGenerator(name = "increment", strategy = "increment")
	private int idCentro;
	
	@Column(name="nombreCentro")
	private String nombreCentro;
	
	@Column(name="numPacientes")
	private int numPacientes;

	//el uno es el que mapea
	
	//Un centro tiene varias habitaciones
	@OneToMany(mappedBy="centro") //centro es el mismo nombre que el atributo de la clase Habitaciones
	private List<Habitaciones> listaHabitaciones;
	
	//Un centro tiene varios usuarios
	@OneToMany (mappedBy="centro")
	private List <Usuarios> listaUsuarios;
	
	//Un centro tiene varios trabajadores
	@OneToMany (mappedBy="centro")
	private List<Trabajadores> listaTrabajadores;
	

	
	public List<Usuarios> getListaUsuarios() {
		return listaUsuarios;
	}
	public void setListaUsuarios(List<Usuarios> listaUsuarios) {
		this.listaUsuarios = listaUsuarios;
	}
	public List<Trabajadores> getListaTrabajadores() {
		if(listaTrabajadores==null) {
			listaTrabajadores = new ArrayList<Trabajadores>();
		}
		return listaTrabajadores;
	}
	public void setListaTrabajadores(List<Trabajadores> listaTrabajadores) {
		this.listaTrabajadores = listaTrabajadores;
	}

	
	public Centros() {
		super();
	}
	public Centros(int idUsuario, int idCentro, String nombreCentro, int numPacientes) {
		super();
		this.idUsuario = idUsuario;
		this.idCentro = idCentro;
		this.nombreCentro = nombreCentro;
		this.numPacientes = numPacientes;
	}

	public Centros(String string, int j) {
		
		this.nombreCentro=string;
		this.numPacientes=j;
	}

	public int getIdCentro() {
		return idCentro;
	}
	
	public int getNumPacientes() {
		return numPacientes;
	}
	public void setNombreCentro(String nombreCentro) {
		this.nombreCentro = nombreCentro;
	}

	public void setNumPacientes(int numPacientes) {
		this.numPacientes = numPacientes;
	}
	
	public String getNombreCentro() {
		return nombreCentro;
	}


	public int getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(int idUsuario) {
		this.idUsuario = idUsuario;
	}

	public void setIdCentro(int idCentro) {
		this.idCentro = idCentro;
	}
	public List<Habitaciones> getListaHabitaciones() {
		if(listaHabitaciones==null) {
			listaHabitaciones = new ArrayList<Habitaciones>();
		}
		return listaHabitaciones;
	}
	public void setListaHabitaciones(List<Habitaciones> listaHabitaciones) {
		this.listaHabitaciones = listaHabitaciones;
	}
}
