package es.unex.cum.mdai.pract1.clases;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "HABITACIONES")
public class Habitaciones {
	@Id
	@GeneratedValue(generator = "increment")
	@GenericGenerator(name = "increment", strategy = "increment")
	private int idHabitacion;
	
	@Column(name="planta")
	private int planta;
	
	@Column(name="numPacientesHabitacion")
	private int numPacientesHabitacion;
	
	@OneToMany(mappedBy="habitacion")
	private List<Pacientes> listaPacientes=new LinkedList<Pacientes>();//Una habitacion tiene N pacientes

	@ManyToOne
	@JoinColumn(name="idCentro") //idCentro es el atributo que comparten centros y habitaciones
	private Centros centro;
	
	
	public Habitaciones() {
		super();
	}
	public Habitaciones(int idHabitacion, int planta, int numPacientesHabitacion, List<Pacientes> listaPacientes,
			Centros centro) {
		super();
		this.idHabitacion = idHabitacion;
		this.planta = planta;
		this.numPacientesHabitacion = numPacientesHabitacion;
		this.listaPacientes = listaPacientes;
		this.centro = centro;
	}
	public Centros getCentro() {
		return centro;
	}
	public void setCentro(Centros centro) {
		this.centro = centro;
	}
	public int getIdHabitacion() {
		return idHabitacion;
	}
	public void setIdHabitacion(int idHabitacion) {
		this.idHabitacion = idHabitacion;
	}
	
	public int getPlanta() {
		return planta;
	}
	public void setPlanta(int planta) {
		this.planta = planta;
	}
	public List<Pacientes> getListaPacientes() {
		if(listaPacientes==null) {
			listaPacientes = new ArrayList<Pacientes>();
		}
		return listaPacientes;
	}
	public void setListaPacientes(List<Pacientes> listaPacientes) {
		this.listaPacientes = listaPacientes;
	}
	public int getNumPacientesHabitacion() {
		return numPacientesHabitacion;
	}
	public int getnumPacientesHabitacion() {
		return numPacientesHabitacion;
	}
	public void setNumPacientesHabitacion(int numPacientesHabitacion) {
		this.numPacientesHabitacion = numPacientesHabitacion;
	}
	
	
}
