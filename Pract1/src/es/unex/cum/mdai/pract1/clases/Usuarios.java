package es.unex.cum.mdai.pract1.clases;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "USUARIOS")
public abstract class Usuarios {
	@Id
	@GeneratedValue(generator = "increment")
	@GenericGenerator(name = "increment", strategy = "increment")
	protected int idUsuario;
	@Column(name="nombre")
	protected String nombre;
	@Column(name="apellidos")
	protected String apellidos;

	
	//Un centro tiene muchos usuarios y un usuario solo pertenece a un centro 
	@ManyToOne
	@JoinColumn(name="idCentro") //idCentro es el atributo que comparten centros y usuarios
	private Centros centro;
	
	public Usuarios() {
		super();
	}
	public Usuarios(String nombre, String apellidos) {
		this.apellidos=apellidos;
		this.nombre=nombre;
	}
	public Usuarios(int idUsuario, String nombre, String apellidos) {
		super();
		this.idUsuario = idUsuario;
		this.nombre = nombre;
		this.apellidos=apellidos;
	}
	

	
	public Centros getCentro() {
		return centro;
	}
	public void setCentro(Centros centro) {
		this.centro = centro;
	}
	public int getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(int idUsuario) {
		this.idUsuario = idUsuario;
	}

	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public String getApellidos() {
		return apellidos;
	}
	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}
}
