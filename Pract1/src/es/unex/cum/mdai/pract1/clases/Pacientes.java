package es.unex.cum.mdai.pract1.clases;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@PrimaryKeyJoinColumn(name="idUsuario")
@Table(name = "PACIENTES")

public class Pacientes extends Usuarios {
	@Column(name="edad")
	private int edad;
	@ManyToOne
	@JoinColumn(name="idHabitacion") //idHabitacion es el atributo que comparten pacientes y habitaciones
	private Habitaciones habitacion; //Un paciente tiene una habitacion

	@ManyToMany(mappedBy = "listaPacientes")
	private List<Tratamiento> listaTratamientos; //Muchos pacientes tienen muchos tratamientos

	@ManyToMany (mappedBy="listaPacientes")
	private List<Familiares> listaFamiliares;
	
	public List<Familiares> getListaFamiliares() {
		return listaFamiliares;
	}

	public void setListaFamiliares(List<Familiares> listaFamiliares) {
		this.listaFamiliares = listaFamiliares;
	}

	public int getEdad() {
		return edad;
	}

	public void setEdad(int edad) {
		this.edad = edad;
	}

	public Habitaciones getHabitacion() {
		return habitacion;
	}

	public void setHabitacion(Habitaciones habitacion) {
		this.habitacion = habitacion;
	}

	public List<Tratamiento> getListaTratamientos() {
		return listaTratamientos;
	}

	public void setListaTratamientos(List<Tratamiento> listaTratamientos) {
		this.listaTratamientos = listaTratamientos;
	}

	public Pacientes(int edad, Habitaciones habitacion, List<Tratamiento> listaTratamientos,
			List<Familiares> listaFamiliares) {
		super();
		this.edad = edad;
		this.habitacion = habitacion;
		this.listaTratamientos = listaTratamientos;
		this.listaFamiliares = listaFamiliares;
	}

	public Pacientes() {
		super();
	}






}
