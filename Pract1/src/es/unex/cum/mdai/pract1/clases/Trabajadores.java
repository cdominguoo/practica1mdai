package es.unex.cum.mdai.pract1.clases;

import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@PrimaryKeyJoinColumn(name="idUsuario")
@Table(name = "TRABAJADORES")
public class Trabajadores extends Usuarios {
		
	public Trabajadores() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Trabajadores(Centros centro, String nombre, String apellidos) {
		super(nombre, apellidos);
		// TODO Auto-generated constructor stub
	}
	public Trabajadores(int idUsuario, String nombre, String apellidos) {
		super(idUsuario, nombre, apellidos);
		// TODO Auto-generated constructor stub
	}

}
