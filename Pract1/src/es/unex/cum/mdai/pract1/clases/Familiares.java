package es.unex.cum.mdai.pract1.clases;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@PrimaryKeyJoinColumn(name="idUsuario")
@Table(name = "FAMILIARES")
public class Familiares extends Usuarios {
	@Column(name="parentesco")
	private String parentesco;
	@ManyToMany
	private List<Pacientes> listaPacientes; //Varios familiares pueden tener varios pacientes
	

	public List<Pacientes> getListaPacientes() {
		return listaPacientes;
	}

	public void setListaPacientes(List<Pacientes> listaPacientes) {
		this.listaPacientes = listaPacientes;
	}
	
	public String getParentesco() {
		return parentesco;
	}

	public void setParentesco(String parentesco) {
		this.parentesco = parentesco;
	}

	public Familiares(String parentesco, List<Pacientes> listaPacientes) {
		super();
		this.parentesco = parentesco;
		this.listaPacientes = listaPacientes;
	}

	public Familiares() {
		super();
	}




}
