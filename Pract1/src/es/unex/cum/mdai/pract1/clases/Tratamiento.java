package es.unex.cum.mdai.pract1.clases;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
@Entity
@Table(name = "TRATAMIENTO")
public class Tratamiento {
	@Id
	@GeneratedValue(generator = "increment")
	@GenericGenerator(name = "increment", strategy = "increment")
	private int idTratamiento;
	@Column(name="nombreTratamiento")
	private String nombreTratamiento;
	@Column(name="dosis")
	private int dosis;
	@Column(name="vecesDia")
	private int vecesDia;
	
	
	@ManyToMany
	private List<Pacientes> listaPacientes; //Los tratamientos los pueden tener varios pacientes
	
	
	public int getIdTratamiento() {
		return idTratamiento;
	}
	public void setIdTratamiento(int idTratamiento) {
		this.idTratamiento = idTratamiento;
	}
	public String getNombreTratamiento() {
		return nombreTratamiento;
	}
	public void setNombreTratamiento(String nombreTratamiento) {
		this.nombreTratamiento = nombreTratamiento;
	}
	public int getDosis() {
		return dosis;
	}
	public void setDosis(int dosis) {
		this.dosis = dosis;
	}
	public int getVecesDia() {
		return vecesDia;
	}
	public void setVecesDia(int vecesDia) {
		this.vecesDia = vecesDia;
	}
	public List<Pacientes> getListaPacientes() {
		return listaPacientes;
	}
	public void setListaPacientes(List<Pacientes> listaPacientes) {
		this.listaPacientes = listaPacientes;
	}
	public Tratamiento(int idTratamiento, String nombreTratamiento, int dosis, int vecesDia,
			List<Pacientes> listaPacientes) {
		super();
		this.idTratamiento = idTratamiento;
		this.nombreTratamiento = nombreTratamiento;
		this.dosis = dosis;
		this.vecesDia = vecesDia;
		this.listaPacientes = listaPacientes;
	}
	public Tratamiento() {
		super();
	}
	

}
