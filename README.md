# Pr�ctica1MDAI
Pr�ctica 1.- Clara Dom�nguez Cidoncha

-Aclaraciones relaciones pr�ctica:

Un centro tiene N trabajadores y un trabajador solo pertenece a un centro.
	Casos de prueba relaci�n centros-trabajadores: CentrosDaoTest

Un centro tiene N habitaciones y una habitaci�n solo pertenece a un centro. 
	Casos de prueba relaci�n centros-habitaciones: HabitacionesDaoTest

Una habitaci�n tiene N pacientes y un paciente solo pertenece a una habitaci�n.
	Casos de prueba relaci�n habitaciones-pacientes: PacientesDaoTest

Un centro tiene N pacientes y un paciente solo pertenece a un centro.
	Casos de prueba relaci�n pacientes-centro: PacientesDaoTest 

Varios pacientes pueden tener varios tratamientos y varios tratamientos pueden tener varios pacientes (N-M).
	Casos de prueba relaci�n pacientes-tratamientos: TratamientosDaoTest

Varios pacientes pueden tener varios familiares y varios familiares pueden tener varios pacientes. (N-M)
	Casos de prueba relaci�n pacientes-familiares: FamiliaresDaoTest

